#include "DistanceConstraint.h"
#include <iostream>

using namespace std;

///
/// GLOBAL METHOD
///

void DistanceConstraint::addConstantTerms(Matrix &b) {
	b(0, 0) -= Constraint::diffEqSignum() * (1.0f/ tau * firstDerivative());
	b(0, 0) -= abs(firstDerivative() + 1.0f / tau * getOffset());
}

void DistanceConstraint::addForceCoefficients(Matrix &m) {
	Vector u = secondDerivative();
	u *= Constraint::diffEqSignum();
	for (int i = 0; i < 2; i++) {
		m(0, 3 * firstBody->index + i) += u[i];
		m(0, 3 * secondBody->index + i) -= u[i];
	}
}

///
/// LOCAL METHOD
///

void DistanceConstraint::addConstantTermsLocal(Matrix &b) {
	b(index, 0) -= 2.0f / tau * firstDerivative();
	b(index, 0) -= 1.0f / (tau * tau) * getOffset();
	if (abs(b(index, 0)) < SimMath::EPS)
		b(index, 0) = 0;
}


void DistanceConstraint::addForceCoefficientsLocal(Matrix &m) {
	Vector u = secondDerivative();
	for (int i = 0; i < 2; i++) {
		m(index, 3 * firstBody->index + i) += u[i];
		m(index, 3 * secondBody->index + i) -= u[i];
	}
}

///
/// SPARSE METHOD
///

void DistanceConstraint::addForceCoefficientsSparse(SparseMatrix &m) {
	Vector u = secondDerivative();
	for (int i = 0; i < 2; i++) {
		m.coeffRef(index, 3 * firstBody->index + i) += u[i];
		m.coeffRef(index, 3 * secondBody->index + i) -= u[i];
	}
}

///
/// CONSTRAINT SPECIFIC MATH METHODS
///

float DistanceConstraint::getOffset() {
	Vector p1 = firstBody->position;
	Vector p2 = secondBody->position;
	return (p1 - p2).norm() - radius;
}

float DistanceConstraint::firstDerivative() {
	// Position variables
	Vector p1 = firstBody->position;
	Vector p2 = secondBody->position;
	// Velocity variables
	Vector v1 = firstBody->velocity;
	Vector v2 = secondBody->velocity;
	float length = (p1 - p2).norm() + SimMath::EPS;
	return (v1 - v2).dot(p1 - p2) / length;
}

Vector DistanceConstraint::secondDerivative() {
	// Position variables
	Vector p1 = firstBody->position;
	Vector p2 = secondBody->position;
	float length = (p1 - p2).norm() + SimMath::EPS;
	return (p1 - p2) / length;
}