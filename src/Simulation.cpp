#include "Simulation.h"

#include <iostream>

using namespace std;

Simulation::Simulation() {
	bodies = vector<Body *>();
	constraints = vector<Constraint *>();
	clockTime = 0.0f;
	tau = 10.0f;
}

Simulation::~Simulation() {
}

void Simulation::stepVelocities(float dt) {
	for (Body *bi : bodies)
		bi->position += bi->velocity * dt;
}

void Simulation::stepPositions(float dt) {
	for (Body *bi : bodies)
		bi->velocity += bi->force * dt;
}

void Simulation::stepForces(float dt, int mode) {
	if (mode == MODE_GLOBAL)
		stepForcesGlobal(dt);
	else if (mode == MODE_LOCAL)
		stepForcesLocal(dt);
	else if (mode == MODE_SPARSE)
		stepForcesSparse(dt);
}	

void Simulation::stepForcesGlobal(float dt) {
	int n = bodies.size();
	Matrix A = Matrix::Zero(1, 3*n);
	Matrix F = Matrix::Zero(n * 3, 1);
	Matrix b = Matrix::Zero(1, 1);

	for (Constraint* ci : constraints) {
		ci->addConstantTerms(b);
		ci->addForceCoefficients(A);
	}
	// stop assembly if b is really small
	if (abs(b(0,0)) < SimMath::EPS) {
		b(0, 0) = 0.0f;
	}
	Solvers::solveDot(A, F, b);
	// update forces on bodies
	cout << b << endl;
	for (Body* bi : bodies) {
		int index = bi->index;
		bi->force[0] = -F(3 * index, 0);
		bi->force[1] = -F(3 * index + 1, 0);
		bi->force[2] = -F(3 * index + 2, 0);
		// cout << bi->force << endl;
	}
}


void Simulation::stepForcesLocal(float dt) {
	int n = bodies.size();
	int m = constraints.size();

	Matrix A = Matrix::Zero(m, n * 3);
	Matrix F = Matrix::Zero(n * 3, 1);
	Matrix b = Matrix::Zero(m, 1);

	for (Constraint* ci : constraints) {
		ci->addConstantTermsLocal(b);
		ci->addForceCoefficientsLocal(A);
		// time dependent variable
	}

	Solvers::solveLinear(A, F, b);

	for (Body* bi : bodies) {
		int index = bi->index;
		bi->force[0] = F(3 * index, 0);
		bi->force[1] = F(3 * index + 1, 0);
		bi->force[2] = F(3 * index + 2, 0);
	}
}


void Simulation::stepForcesSparse(float dt) {
	int n = bodies.size();
	int m = constraints.size();

	SparseMatrix A = SparseMatrix(m, n * 3);
	Matrix F = Matrix::Zero(n * 3, 1);
	Matrix b = Matrix::Zero(m, 1);

	for (Constraint* ci : constraints) {
		ci->addConstantTerms(b);
		ci->addForceCoefficientsSparse(A);
		// time dependent variable
	}

	Solvers::solveLinearSparse(A, F, b);

	for (Body* bi : bodies) {
		int index = bi->index;
		bi->force[0] = F(3 * index, 0);
		bi->force[1] = F(3 * index + 1, 0);
		bi->force[2] = F(3 * index + 2, 0);
	}
}


void Simulation::initSimulation() {
	assignIndices();
	initTau();
}

void Simulation::assignIndices() {
	int ind = 0;
	for (Body *bi : bodies) {
		bi->index = ind;
		ind++;
	}
	ind = 0;
	for (Constraint *ci : constraints) {
		ci->index = ind;
		ind++;
	}
}

void Simulation::initTau() {
	for (Constraint *ci : constraints)
		ci->tau = tau;

}

void Simulation::step(float dt) {
	clockTime += dt;
	stepPositions(dt);
	stepVelocities(dt);
	stepForces(dt, mode);
}