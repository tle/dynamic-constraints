#include "Solvers.h"

#include <iostream>

using namespace std;
void Solvers::solveLinear(Matrix A, Matrix &x, Matrix b) {
	Eigen::JacobiSVD<Matrix, Eigen::HouseholderQRPreconditioner> solver(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
	x = solver.solve(b);
}

void Solvers::solveLinearSparse(SparseMatrix A, Matrix &x, Matrix b) {
	SparseMatrix M = A.transpose() * A;
	Matrix bp = A.transpose() * b;
	Eigen::BiCGSTAB<SparseMatrix> solver(M);
	x = solver.solve(bp);
}

void Solvers::solveDot(Matrix A, Matrix &x, Matrix b) {
	float temp = (A * A.transpose())(0, 0);

	x = (A.transpose() * b.norm()) / temp;

}