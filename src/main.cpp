
#include "Viewer.h"
#include "Simulation.h"
#include "Types.h"
#include "DistanceConstraint.h"

#include <cmath>
#include <iostream>
#include <cstdlib>

#include <ctime>
#include <random>

using namespace std;

typedef std::mt19937 t_RNG;

t_RNG rng;

int main(int argc, char *argv[]) {
	Simulation *s = new Simulation();

	if (argc != 4) {
		cout << "Wrong input! Usage:" << endl;
		cout << "./bin/main [mode] [number of particles] [timestep size]" << endl;
		return 0;
	}
	
	int mode = atoi(argv[1]);
	int N = atoi(argv[2]);
	float dt = atof(argv[3]);
	
	// Initialize random number generator
	int rngSeed = time(0);
	rng.seed(rngSeed);

	std::uniform_real_distribution<float> dist(0,100); // range [0,10]
	// generate some bodies	
	//Vector vec;
	//vec << 0.0f, 0.0f, 0.0f;
	float rad = 0;
	rad += dist(rng);
	for (int i = 0; i < N; i++) {
		Vector u;
		//rad = dist(rng);
		u[0] = dist(rng) * sin(i * 2 * 3.14 / N);
		u[1] = dist(rng) * cos(i * 2 * 3.14 / N);
		u[2] = 0.0f;
		s->addBody(new Body(u));
	}

	// assign indices
	s->assignIndices();
	// generate some constraints
	s->setMode(mode);
	for (int i = 1; i < N; i++) {
		s->addConstraint(new DistanceConstraint(s->bodies[i], s->bodies[i-1]));
	}

	//s->addConstraint(new DistanceConstraint(s->bodies[0], s->bodies[N - 1]));

	Viewer v;
	v.assign(s);
	v.initWindow(600, 600);
	v.setCameraOrtho(-10, 10, -10, 10, 0.1, 500);
	v.setTimestep(dt);
	v.loop();
}