#include "Body.h"

#include <iostream>

int main(int argc, char **argv) {


	Body blob;
	Quad(1, 2, 3, 4);
	Tensor t;
	t << 1, 2, 3,
		4, 5, 6,
		7, 8, 9;
	Vector a(3, 3, 3);
	Vector b(1, -11, 21);

	std::cout << t << std::endl;
	std::cout << a + b << std::endl;
}