#include "Viewer.h"
#include <iostream>

Viewer::Viewer() {
	// TODO: Think about what to put here
	timestep = 1.0;
}

void Viewer::init() {
    // Flags for some features
    // Shading
    glShadeModel(GL_SMOOTH);
    // Depth Test
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}

void Viewer::initLights() {
    GLfloat amb[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat diff[]= { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat spec[]= { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat lightpos[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat shiny = 10.0f; 

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);

    int nLights = 1;
    for (int i = 0; i < nLights; i++) {
		float r = i * 2 * 3.14 / nLights;
		lightpos[0] = 12.0f * sin(r);
	    lightpos[1] = 12.0f * cos(r);
	    lightpos[2] = 3.0f;

		glLightfv(GL_LIGHT0 + i, GL_AMBIENT, amb);
	    glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, diff);
	    glLightfv(GL_LIGHT0 + i, GL_SPECULAR, spec);
	    glLightfv(GL_LIGHT0 + i, GL_POSITION, lightpos);
	    glLightf(GL_LIGHT0 + i, GL_SHININESS, shiny);
	    glEnable(GL_LIGHT0 + i);
	}
    // Turn on lighting.  You can turn it off with a similar call to
    // glDisable().
    glEnable(GL_LIGHTING);

}

void Viewer::initMaterial() {
    GLfloat emit[] = {0.0, 0.0, 0.0, 1.0};
    GLfloat  amb[] = {0.1, 0.1, 0.1, 1.0};
    GLfloat diff[] = {0.0, 0.0, 1.0, 1.0};
    GLfloat spec[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat shiny = 4.0f;

    glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diff);
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_EMISSION, emit);
    glMaterialfv(GL_FRONT, GL_SHININESS, &shiny);
}

void Viewer::initWindow(int w, int h) {
	init();
	width = w;
	height = h;
	glfwInit();
	glfwOpenWindow(width, height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);

}

void Viewer::setCameraOrtho(float left, float right, float bottom,
						float top, float near, float far)
{
	// Scene set up
    // Set up the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(left, right, bottom, top, near, far);
}

void Viewer::setTimestep(float dt) {
	timestep = dt;
}

void Viewer::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	initLights();
    initMaterial();

	GLUquadricObj *quad = gluNewQuadric();
	Vector avgPos = Vector::Zero();
	for (Body *b : sim->bodies)
		avgPos += b->position;
	avgPos /= sim->bodies.size();
	
	float x = avgPos[0];
	float y = avgPos[1];
	float z = avgPos[2];
	gluLookAt(x - 10.0f, y - 10.0f, z - 10.0f, x, y, z, 0.0f, 1.0f, 0.0f);

    for (Body *b : sim->bodies) {
		glPushMatrix();
		glTranslatef(b->position[0], b->position[1], b->position[2]);
		gluSphere(quad, 1.0, 50, 50);
		glPopMatrix();
	}

	gluDeleteQuadric(quad);
	glfwSwapBuffers();
}

void Viewer::loop() {
	int running = GL_TRUE;
	sim->initSimulation();

    while (running) {
    	// update simulation!
		typedef std::chrono::duration<float, std::milli> ms;
    	
    	auto start = std::chrono::high_resolution_clock::now();
    	sim->step(timestep);
    	auto end = std::chrono::high_resolution_clock::now();
    	

    	cout << "ms per frame: " << 
    			std::chrono::duration_cast<ms>(end - start).count()	
    			<< endl;

        render();
        // check whether user pressed escape
        running = handleKeyPresses();
    }
    glfwTerminate();    
}


bool Viewer::handleKeyPresses() {
	if (glfwGetKey(GLFW_KEY_ESC))
		return false;
	if (glfwGetKey('+'))
		timestep += 0.1f;
	if (glfwGetKey('-'))
		timestep -= 0.1f;
	if (timestep < 0.0f)
		timestep = 0.0f;
	else if (timestep > 5.0f)
		timestep = 5.0f;
	return true;
}

void Viewer::assign(Simulation *s) {
	sim = s;
}