CXX			= g++
CXXFLAGS    = -Wall -g -std=c++0x
LIBS		= $(OPENGL_LIBS)
# WINDOWS
OPENGL_LIBS		= -lglfw -lopengl32 -lglu32
SHELL       = C:/Windows/System32/cmd.exe
INCLUDES	= -I./include
# Linux
# LIBS		= -lglfw -lGLU -lGL
# INCLUDES	= -I./include -L/usr/X11R6/lib

CFLAGS		= $(CXXFLAGS) $(INCLUDES)
LFLAGS		= $(CXXFLAGS)

TARGET		= bin/main

OBJS		= objs/main.o objs/Viewer.o objs/DistanceConstraint.o objs/Simulation.o objs/Solvers.o

MKDIR_P		= mkdir -p
.PHONY		= directories
OUT_DIR		= bin objs
directories	= $(OUT_DIR)

all:		directories $(TARGET)

directories:	$(OUT_DIR)

$(OUT_DIR):
		$(MKDIR_P) $(OUT_DIR)

$(TARGET): $(OBJS)
		$(CXX) $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

objs/main.o: src/main.cpp
		$(CXX) $(CFLAGS) -c src/main.cpp -o objs/main.o

objs/Body.o: src/Body.cpp
		$(CXX) $(CFLAGS) -c src/Body.cpp -o objs/Body.o

objs/Viewer.o: src/Viewer.cpp
		$(CXX) $(CFLAGS) -c src/Viewer.cpp -o objs/Viewer.o

objs/Constraint.o: src/Constraint.cpp
		$(CXX) $(CFLAGS) -c src/Constraint.cpp -o objs/Constraint.o

objs/DistanceConstraint.o: src/DistanceConstraint.cpp
		$(CXX) $(CFLAGS) -c src/DistanceConstraint.cpp -o objs/DistanceConstraint.o

objs/PointConstraint.o: src/PointConstraint.cpp
		$(CXX) $(CFLAGS) -c src/PointConstraint.cpp -o objs/PointConstraint.o


objs/Simulation.o: src/Simulation.cpp
		$(CXX) $(CFLAGS) -c src/Simulation.cpp -o objs/Simulation.o

objs/Solvers.o: src/Solvers.cpp
		$(CXX) $(CFLAGS) -c src/Solvers.cpp -o objs/Solvers.o

clean:
	rm -f $(OBJS)
	rm -f $(TARGET)
