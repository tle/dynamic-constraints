
#ifndef SIMULATION_H
#define SIMULATION_H

#include "Constraint.h"
#include "Body.h"
#include "Solvers.h"

#include <vector>
#include <cmath>

using namespace std;

class Simulation {
	private:
		void stepVelocities(float dt);
		void stepPositions(float dt);
		void stepForces(float dt, int mode);

		void stepForcesLocal(float dt);
		void stepForcesGlobal(float dt);
		void stepForcesSparse(float dt);


		void initTau();
		float clockTime;

		float tau;

		float mode;
		
	public:
		Simulation();
		~Simulation();

		vector<Constraint *> constraints;
		vector<Body *> bodies;

		enum SIM_MODE {
			MODE_GLOBAL,
			MODE_LOCAL,
			MODE_SPARSE
		};

		void assignIndices();

		void addConstraint(Constraint *a) {
			constraints.push_back(a);
		};

		void addBody(Body *b) {
			bodies.push_back(b);
		};


		void initSimulation();
		void step(float dt);

		void setMode(int m) {
			mode = m;
		};

};

#endif