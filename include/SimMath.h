#ifndef SIM_MATH_H
#define SIM_MATH_H

namespace SimMath {
	const float EPS = 0.0001f;

	inline int signum(float val) {
		if (val > EPS)
			return 1.0f;
		else if (val < -EPS)
			return -1.0f;
		else
			return 0.0f;
	}
}


#endif