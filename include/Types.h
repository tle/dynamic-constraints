#include <Eigen/Core>
#include <Eigen/Sparse>

#ifndef TYPES_H
#define TYPES_H

#include <iterator>

typedef Eigen::Matrix<float, 1, 3> Vector;
typedef Eigen::Matrix<float, 1, 4> Quad;
typedef Eigen::Matrix<float, 3, 3> Tensor;
typedef Eigen::MatrixXf Matrix;
typedef Eigen::SparseMatrix<float> SparseMatrix;

#endif