#ifndef VIEWER_H
#define VIEWER_H

#include "Simulation.h"

// OpenGL dependencies
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glfw.h>

#include <chrono>

class Viewer {
	private:
		Simulation *sim;
		int width;
		int height;
		float timestep;

		bool handleKeyPresses();
		void render();
		
		void init();
		void initLights();
		void initMaterial();
	public:
		Viewer();
		~Viewer() {
			delete sim;
		}
		
		// Initializes OpenGL and all that cra
		void initWindow(int width, int height);

		// Set camera parameters for the viewing space
		void setCameraOrtho(float left, float right, float bottom, float top, float near, float val);

		// Changes timestep taken per frame
		void setTimestep(float dt);

		// Assigns a simulation to the viewer
		void assign(Simulation *s);

		void loop();

};


#endif