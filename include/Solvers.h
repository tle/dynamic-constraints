#ifndef SolversRS_H
#define SOLVERS_H

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SVD>

#include "Types.h"

namespace Solvers
{
	void solveLinear(Matrix A, Matrix &x, Matrix b);
	void solveLinearSparse(SparseMatrix A, Matrix &x, Matrix b);
	void solveDot(Matrix A, Matrix &x, Matrix b);
}

#endif