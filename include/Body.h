/*
 * A class for reprsenting a body in the simulation
 * Specified by the Barr, Barzel paper
 */

#ifndef BODY_H
#define BODY_H

 #include <vector>

class Body { 
	public:
		// Constructor
		Body();
		// Constructor with position
		Body(const Vector p) : position(p) {
			velocity = Vector::Zero();
			force = Vector::Zero();
		} 
		// Destructor
		~Body() {};
		// Vector of the position of the body
		Vector position;
		// Velocity of the body
		Vector velocity;
		// Force acting on the body
		Vector force;
		// index of the body
		int index;
};

#endif