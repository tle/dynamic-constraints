#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <vector>
#include <iterator>
#include "Types.h"
#include "Body.h"
#include "SimMath.h"

#include <iostream>
// general superclass for the constraint type
class Constraint {
	public:
		int index;
		// Constructor of the class
		Constraint() { };
		// Destructor
		virtual ~Constraint() { };

		virtual void addConstantTerms(Matrix &b) = 0;
		virtual void addForceCoefficients(Matrix &m) = 0;

		virtual void addConstantTermsLocal(Matrix &b) = 0;
		virtual void addForceCoefficientsLocal(Matrix &m) = 0;

		virtual void addForceCoefficientsSparse(SparseMatrix &m) = 0;

		virtual float getOffset() = 0;
		// derivative functions

		virtual float firstDerivative() = 0;
		virtual Vector secondDerivative() = 0;

		/*
		Computes the sign of the constraint differential equation expression
		Dij = dij' + 1/tau * dij
		*/
		float diffEqSignum() {
			return SimMath::signum(firstDerivative() + 1.0f / tau * getOffset());
		}
		/*
		Computes the sign of the constraint
		dij = |xi - xj| - radius
		*/ 
		float constraintSignum() {
			return SimMath::signum(getOffset());
		}

		float tau;
};

#endif