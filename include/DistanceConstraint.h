#ifndef DISTCONSTRAINT_H
#define DISTCONSTRAINT_H

#include "Constraint.h"
#include "Types.h"
#include <cmath>

class DistanceConstraint: public Constraint {
	private:
		Body *firstBody;
		Body *secondBody;
		float radius;
	public:
		DistanceConstraint(Body *b1, Body *b2) {
			firstBody = b1;
			secondBody = b2;
			radius = 2.0f;
		};
		
		~DistanceConstraint() {
			delete firstBody;
			delete secondBody;
		};

		void addConstantTerms(Matrix &b);
		void addForceCoefficients(Matrix &m);

		void addConstantTermsLocal(Matrix &b);
		void addForceCoefficientsLocal(Matrix &m);

		void addForceCoefficientsSparse(SparseMatrix &m);

		float firstDerivative();
		Vector secondDerivative();

		float getOffset();
};

#endif